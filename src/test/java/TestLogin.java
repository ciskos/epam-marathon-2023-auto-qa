import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class TestLogin {

	private static WebDriver driver;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		driver = new ChromeDriver();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
//		driver.quit();
	}

	@BeforeEach
	void setUp() throws Exception {
		driver.get("https://rozetka.com.ua");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	@DisplayName("Check User is able to create new account")
	void test() {
//		<use href="#icon-user-simple"></use>
//		driver.findElement(By.className("header__button ng-star-inserted")).click();
		driver.findElement(By.className("header-actions__item header-actions__item--user")).submit();
	}

}
