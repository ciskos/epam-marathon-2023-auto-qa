import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class TestWashMachines {

	private static WebDriver driver;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
//		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
		driver = new ChromeDriver();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
//		driver.quit();
	}

	@BeforeEach
	void setUp() throws Exception {
		driver.get("https://bt.rozetka.com.ua/washing_machines/c80124/");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	@DisplayName("Check if it possible to enter wash mashine info")
	void test() throws InterruptedException {
//		<use href="#icon-user-simple"></use>
//		<a apprzroute="" rzadvnofollow="" class="goods-tile__picture ng-star-inserted" href="https://bt.rozetka.com.ua/aeg-7332543747788/p350220753/"><img loading="lazy" alt="Стиральная машина AEG L6SE26SUE" title="Стиральная машина AEG L6SE26SUE" src="https://content.rozetka.com.ua/goods/images/big_tile/372283920.jpg" class="ng-lazyloaded"><img loading="lazy" class="lazy_img_hover ng-lazyloaded display-none" alt="Стиральная машина AEG L6SE26SUE" title="Стиральная машина AEG L6SE26SUE" src="https://content.rozetka.com.ua/goods/images/big_tile/372283929.jpg"><ul class="super-actions"><li class="super-actions__item ng-star-inserted"><img loading="lazy" alt="Воспользуйтесь скидкой 5%" class="ng-failed-lazyloaded ng-lazyloaded ng-star-inserted" src="https://content2.rozetka.com.ua/goods_tags/images/original/373797127.png"><!----><!----><!----></li><!----></ul><!----><!----></a>
//		driver.findElement(By.className("header__button ng-star-inserted")).click();
//		driver.findElement(By.className("catalog-grid__cell catalog-grid__cell_type_slim ng-star-inserted")).click();
//		driver.findElement(By.className("ng-star-inserted")).click();
//		driver.findElement(By.className("goods-tile ng-star-inserted")).click();
//		List<WebElement> findElements = driver.findElements(By.className("goods-tile__inner")); // - уходит в постоянную проверку
		List<WebElement> findElements = driver.findElements(By.className("goods-tile__content").tagName("a")); // - уходит в постоянную проверку
		
//		for (WebElement webElement : findElements) {
			var webElement = findElements.get(0);
			System.out.println("isDisplayed " + webElement.isDisplayed());
			System.out.println("isEnabled " + webElement.isEnabled());
			System.out.println("isSelected " + webElement.isSelected());
			System.out.println("isSelected " + webElement.isSelected());
			System.out.println("getTagName " + webElement.getTagName());
			System.out.println("getText " + webElement.getText());
			var attribute = findElements.get(0).getAttribute("href");
			
			System.out.println("attribute " + attribute);
			
			int i = 0;
			while (i < 10000) {
				i++;
			}
			webElement.click();
//		}
		
//		driver.findElement(By.className("goods-tile__picture ng-star-inserted")).submit();
	}

}
